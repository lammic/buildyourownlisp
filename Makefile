CC = cc
CFLAGS = -std=c99 -Wall -g
LFLAGS = 
FILES = hello_world prompt parsing evaluation error_handling s_expressions q_expressions variables functions
PLATFORM = $(shell uname)

ifeq ($(findstring Darwin,$(PLATFORM)),Darwin)
	LFLAGS += -ledit -lm
endif

all: $(FILES)

%: %.c mpc.c
	$(CC) $(CFLAGS) $^ $(LFLAGS) -o $@
