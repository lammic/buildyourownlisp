#include <stdio.h>
#include <stdlib.h>

#include <editline/readline.h>

int main(int argc, char** argv) {
  puts("Lispy Version 0.0.0.0.1");
  puts("Press Ctrl-c to Exit\n");

  while (1) {
    // prompt and input
    char* input = readline("lispy> ");
    // add input to history
    add_history(input);
    // input back
    printf("No you're a %s\n", input);

    free(input);
  }

  return 0;
}
